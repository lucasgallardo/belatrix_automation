package tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import helpers.Helpers;
import pages.PageFilters;
import pages.PageOrder;
import pages.PageSort;
import pages.PageSearch;
import pages.PageShow;

public class Tests {
	private WebDriver driver;
	@BeforeMethod	
	public void setUp() {
		DesiredCapabilities caps = new DesiredCapabilities();
		System.setProperty("webdriver.chrome.driver", "Drivers/chromedriver");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.navigate().to("https://www.ebay.com/");
		Helpers helper = new Helpers();
		helper.sleepSeconds(4);
	}
	
	@Test
	public void search() {
		//search product type
		PageSearch pageSearch = new PageSearch(driver);
		pageSearch.searching("shoes");
		
		//filter by brand and size
		PageFilters pageFilters = new PageFilters(driver);
		pageFilters.filter("PUMA");
		
		//display results
		PageShow pageShow = new PageShow(driver);
		pageShow.displayed();
		
		//sort result
		PageSort pageSort = new PageSort(driver);
		pageSort.sorter();
		
		//Assert the order taking the first 5 results
		PageOrder pageOrder = new PageOrder(driver);
		pageOrder.takingOrder();
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
	}
	
}
