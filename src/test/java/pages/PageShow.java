package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageShow {
	private WebDriver driver;
	private By textQuantity;
	
	public PageShow(WebDriver driver) {
		this.driver = driver;
		textQuantity=By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[1]/div/div[1]/div/div[3]/h1/span[1]");
	}
	
	public void displayed() {
		String cantidad = driver.findElement(textQuantity).getText();		
		System.out.println("NUMBER OF RESULTS: "+cantidad);
	}

}
