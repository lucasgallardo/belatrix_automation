package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import helpers.Helpers;

public class PageSearch {
	private WebDriver driver;
	private By lookField;
	private By searchButton;
	public PageSearch(WebDriver driver) {
		this.driver = driver;
		lookField = By.name("_nkw");
		searchButton = By.id("gh-btn"); 
	}
	
	public void searching(String product) {
		driver.findElement(lookField).sendKeys(product);
		driver.findElement(searchButton).click();
//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//		Helpers helper = new Helpers();
//		helper.sleepSeconds(5);
	}
}
