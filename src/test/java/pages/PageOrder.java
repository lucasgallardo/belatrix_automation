package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class PageOrder {
	private WebDriver driver;
	private By resultGallery;
	private By itemGallery;
	private By addToCartButton;
	private By sizeSelect;
	private By colorSelect;
	private By cartText;

	public PageOrder(WebDriver driver) {
		this.driver = driver;
		resultGallery=By.id("GalleryViewInner");
		itemGallery=By.tagName("a");
		addToCartButton=By.id("isCartBtn_btn");
		sizeSelect=By.id("msku-sel-1");//By.xpath("//*[@id=\"msku-sel-1\"]");
		colorSelect=By.id("msku-sel-2");
		cartText=By.tagName("h1");
	}
	
	public void takingOrder() {
        String current = driver.getCurrentUrl();
		WebElement resultado = driver.findElement(resultGallery);
		List<WebElement> item = resultado.findElements(itemGallery);

		int j = 1;
		do
		{
			String linkItem=item.get(j).getText();
			System.out.println(linkItem);			
			addCart(current, driver, linkItem);
			j=j+2;

		}while(j<13);
	}
	
	public void addCart(String current, WebDriver driver, String linkCurrentItem) {
		this.driver = driver;	
		
		//open new tab
		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
		driver.findElement(By.linkText(linkCurrentItem)).sendKeys(selectLinkOpeninNewTab);		
		
		//switch to new tab
	    ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		
		
		if (driver.findElements(By.id("bidBtn_btn")).size() != 0) {
			driver.close();			
			driver.switchTo().window(tabs.get(0));	
		}else {
			Select selectSizeShoe = new Select(driver.findElement(sizeSelect));
			selectSizeShoe.selectByValue("0");
			
			if (driver.findElements(colorSelect).size() != 0) {
				Select selectColorShoe = new Select(driver.findElement(colorSelect));
				selectColorShoe.selectByValue("0");
			}
			
			driver.findElement(addToCartButton).click();
			Assert.assertTrue(driver.findElement(cartText).getText().contains("Carro de compras"));
					
			driver.close();			
			driver.switchTo().window(tabs.get(0));			
		}	
	}	
}
