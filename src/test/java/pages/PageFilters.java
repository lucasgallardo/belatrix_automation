package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import helpers.Helpers;

public class PageFilters {
	private WebDriver driver;
	private By brandInput;
	private By brandButton;
	private By sizeButton;
	private By brandText;
	//private By fields;
	
	public PageFilters(WebDriver driver) {
		this.driver = driver;
		brandInput = By.xpath("//*[@id=\"e1-40\"]");//By.id("e1-40");
		brandButton = By.linkText("PUMA");//By.xpath("/html/body/div[5]/div[2]/div[1]/div[3]/div/div/div[1]/div[2]/div[2]/div[2]/div/div[7]/a/span");	
		sizeButton = By.xpath("/html/body/div[5]/div[2]/div[1]/div[3]/div/div/div[1]/div[2]/div[1]/div[2]/div[4]/a/span[1]");
		brandText = By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[1]/div/div[2]/div/span/span/b/span");
	}
	
	public void filter(String brand) {
		driver.findElement(brandInput).sendKeys(brand);
		driver.findElement(brandButton).click();
		Assert.assertTrue(driver.findElement(brandText).getText().contains(brand));
		driver.findElement(sizeButton).click();
	}

}
