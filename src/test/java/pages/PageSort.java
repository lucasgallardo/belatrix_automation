package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageSort {
	private WebDriver driver;
	private By dropDownButton;
	private By optionButton;
	private By resultGallery;
	private By itemGallery;
	private By priceItemGallery;
	
	public PageSort(WebDriver driver) {
		this.driver = driver;
		dropDownButton=By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[1]/div/div[1]/div/div[2]/div[1]/ul[1]/li/div/a");
		optionButton=By.xpath("/html/body/div[5]/div[2]/div[1]/div[1]/div/div[1]/div/div[1]/div/div[2]/div[1]/ul[2]/li[3]/a");
		resultGallery=By.id("GalleryViewInner");
		itemGallery=By.tagName("a");
		priceItemGallery=By.className("bold");
	}
	
	public void sorter() {
		driver.findElement(dropDownButton).click();
		driver.findElement(optionButton).click();
		WebElement resultado = driver.findElement(resultGallery);
		System.out.println(resultado);
		List<WebElement> titleItem = resultado.findElements(itemGallery);
	    List<WebElement> priceItem = resultado.findElements(priceItemGallery);	    

		for (int i = 0; i < 12; i++)
		{
		    System.out.println(titleItem.get(i).getText());
		    System.out.println(priceItem.get(i).getText());
		}
	}

}
